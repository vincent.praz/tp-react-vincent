import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import CreateUser from "./pages/CreateUser";
import Home from "./pages/Home";
import Login from "./pages/Login";
import UserContext from "./contexts/UserContext";
import Profile from "./pages/Profile";

function App() {
    const [user, setUser] = useState({});
    const [posts, setPosts] = useState({});
    const [userView, setUserView] = useState({});
    const [jwt, setJWT] = useState("");

    useEffect(() => {
        const localJWT = localStorage.getItem("jwt");
        const localUser = JSON.parse(localStorage.getItem("user") || "{}");
        const localUserView = JSON.parse(localStorage.getItem("userView") || "{}");
        const localPosts = JSON.parse(localStorage.getItem("posts") || "{}");

        if (localJWT) {
            setJWT(localJWT);
        }

        if (localUser) {
            setUser(localUser);
        }

        if (localUserView) {
            setUserView(localUserView);
        }

        if (localPosts) {
            setPosts(localPosts);
        }
    }, []);

    return (
        <UserContext.Provider value={{posts, setPosts, userView, setUserView, user, setUser, jwt, setJWT}}>
            <Router>
                {!jwt && (
                    <>
                        <Route path="/" exact component={Login}/>
                        <Route path="/create" exact component={CreateUser}/>
                    </>
                )}

                {!!jwt && (
                    <>
                        <Route path="/" exact component={Home}/>
                        <Route path="/profile" exact component={Profile}/>
                    </>
                )}
            </Router>
        </UserContext.Provider>
    );
}

export default App;
