import React, {useContext} from "react";
import UserContext from "../contexts/UserContext";
import {useHistory} from "react-router-dom";
import styled from 'styled-components';

const UserButton = styled.button`
    color: white;
    background-color: transparent;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;
`;


const Users = (user) => {
    const {setUserView} = useContext(UserContext);
    let history = useHistory();

    const goToProfile = () => {
        // Set the user that we want to see the profile
        setUserView(user.user);

        localStorage.setItem("userView", JSON.stringify(user.user));

        //Redirect to Profile page
        history.push("/profile");
    }

    return (
        <li>
            <UserButton onClick={goToProfile}>{user.user.username}</UserButton>
            {/*TODO: maybe change to links instead of buttons or restyle it */}
        </li>
    )
}

export default Users;
