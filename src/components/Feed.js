import React from "react";
import styled from "styled-components";
import ReactMarkdown from "react-markdown";

const Card = styled.div`
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.8);
  transition: 0.3s;
  border-radius: 5px;
  width: 50%;
  padding:10px;
  margin-top: 25px;
  background-color: #3E403D;
`;


const Feed = (post) => {
    return (
        <Card>
            <h2>{post.post.title}</h2>
            <p><ReactMarkdown source={post.post.content} /></p>
        </Card>
    )
}

export default Feed;
