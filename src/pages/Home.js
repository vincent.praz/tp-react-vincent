import React, {useContext, useEffect, useState} from "react";
import UserContext from "../contexts/UserContext";
import axios from "axios";
import Feed from "../components/Feed";
import Users from "../components/Users";
import {Link, useHistory} from "react-router-dom";
import Input from "../components/Input";
import styled from 'styled-components';


const StyledButtons = styled.button`
    color: white;
    background-color: #3d005c;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;
    border-radius:25px;
    padding : 6px;
`;

const StyledButtonsLogOut = styled.button`
    color: white;
    background-color: #8B0000;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;
    border-radius:25px;
    padding : 6px;
    margin-left:15px;
`;


const SideBar = styled.div`
     height: 100%;
     width: 200px;
     position: fixed;
     z-index: 1;
     top: 0;
     left: 0;
     background-color: #3D005C;
     padding-top: 20px;
`;

const MainContainer = styled.div`
  margin-left: 200px;
  padding: 0px 10px;
`;

const UserList = styled.ul`
    list-style-image: url('/user.png');
`;

const HomeContainer = styled.div`
    color:white;
    position: absolute;
    top:0;
    background-color: #484647;
    padding-left:15px;
`;

const NewPostContainer = styled.div`
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.8);
  background-color: #3E403D;
  transition: 0.3s;
  border-radius: 5px;
  width: 25%;
  padding:10px;
  margin-top: 15px;
`;

const UserInfo = styled.div`
  position:fixed;
  top:15px;
  right:15px;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.8);
  background-color: #3E403D;
  transition: 0.3s;
  border-radius: 5px;
  width: 200px;
  padding:10px;
  margin-top: 15px;
`;

const FloatRight = styled.div`float:right;`;

const Home = () => {
    const {setPosts, user, setUser, jwt, setJWT, setUserView} = useContext(UserContext);
    let history = useHistory();
    // Posts list
    const [displayPosts, setDisplayPosts] = useState([]);

    // New post
    const [newPostTitle, setNewPostTitle] = useState([]);
    const [newPostContent, setNewPostContent] = useState([]);
    const [error, setError] = useState(null);

    // Users list
    const [users, setUsers] = useState([]);

    /**
     * Logs the user out (sets the JWT in the context to null and removes the entry in the localStorage)
     */
    const logOut = () => {
        setJWT(null);
        setUser(null);
        setPosts(null);
        setUserView(null);

        // Empty localstorage
        localStorage.removeItem("jwt");
        localStorage.removeItem("userView");
        localStorage.removeItem("posts");
        localStorage.removeItem("user");
    }

    /**
     * Retrieves the posts using the API
     */
    const getPosts = (jwt) => {
        axios({
            method: "get",
            url: "https://strapi-crea.5ika.org/posts",
            headers: {
                Authorization: `Bearer ${jwt}`,
            }
        })
            .then(function (response) {
                // Set the posts retrieved
                setPosts(response.data);
                setDisplayPosts(response.data);
                localStorage.setItem("posts", JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    /**
     * Retrieves the users using the API
     */
    const getUsers = (jwt) => {
        axios({
            method: "get",
            url: "https://strapi-crea.5ika.org/users",
            headers: {
                Authorization: `Bearer ${jwt}`,
            }
        })
            .then(function (response) {
                console.log(response.data);
                // Set the users retrieved
                setUsers(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    useEffect(() => {
        getPosts(jwt);
        getUsers(jwt);
    }, []);

    const goToProfile = () => {
        // Set the user that we want to see the profile
        setUserView(user);
        localStorage.setItem("userView", JSON.stringify(user));

        //Redirect to Profile page
        history.push("/profile");
    }

    /**
     * Creates post
     * @param title title of the post
     * @param content content of the post
     */
    const createPost = (title, content) => {
        axios({
            method: "post",
            url: "https://strapi-crea.5ika.org/posts",
            headers: {
                Authorization: `Bearer ${jwt}`,
            },
            data: {
                title: title,
                content: content,
                user: user,
            }
        })
            .then(function (response) {
                window.location.reload();
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    /**
     * Validates the form and creates new post
     * */
    const onSubmit = () => {
        setError(null);
        if (!newPostTitle || newPostTitle === "") {
            setError({field: "title", message: "Please enter the post title"});
        } else if (!newPostContent || newPostContent === "") {
            setError({field: "content", message: "Please the post content"});
        } else {
            // Create the post
            createPost(newPostTitle, newPostContent);
            console.log("Post info properly entered");
        }
    }


    return (

        <HomeContainer>
            <SideBar>
                <center><h2>USERS</h2></center>
                <UserList>
                    {users?.map((user, i) => (
                        <Users key={i} user={user}/>
                    ))}
                </UserList>
            </SideBar>
            <MainContainer>
                <h1>HOME PAGE</h1>
                <UserInfo>
                    <p>
                        Connected as {user?.username}
                    </p>
                    <StyledButtons onClick={goToProfile}>Profile</StyledButtons>
                    <StyledButtonsLogOut onClick={logOut}>Log out</StyledButtonsLogOut>
                </UserInfo>

                <h2>NEW POST</h2>

                {/*New post part*/}
                <NewPostContainer>
                    <Input type={"text"}
                           label="Title"
                           error={error?.field === "title"}
                           value={newPostTitle}
                           setValue={setNewPostTitle}/>
                    <br/>
                    <Input type={"text"}
                           label="Content"
                           error={error?.field === "content"}
                           value={newPostContent}
                           setValue={setNewPostContent}/>
                    {error && <div>Error: {error.message}</div>}
                    <FloatRight><StyledButtons onClick={onSubmit}>Submit</StyledButtons></FloatRight>
                </NewPostContainer>

                <h2>FEED</h2>

                {displayPosts?.map((post, i) => (
                    <Feed key={i} post={post}/>
                ))}

            </MainContainer>

        </HomeContainer>
    );
}

export default Home;
