import React, {useContext, useState} from "react";
import Input from "../components/Input";
import {Link} from "react-router-dom";
import UserContext from "../contexts/UserContext";
import axios from "axios";
import styled from "styled-components";

const Login = () => {
    const {setUser, setJWT} = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);


    const StyledButtons = styled.button`
        color: white;
        background-color: #3d005c;
        box-shadow: 0px 0px 0px transparent;
        border: 0px solid transparent;
        text-shadow: 0px 0px 0px transparent;
        border-radius:25px;
        padding : 6px;
    `;

    /**
     * Retrieves the JWT from the API to authenticate the user
     * @param email email of the user
     * @param password password of the user
     */
    const getJWT = (email, password) => {
        axios
            .post("https://strapi-crea.5ika.org/auth/local", {
                identifier: email,
                password: password,
            })
            .then(function (response) {
                // Set the JWT + user retrieved
                setJWT(response.data.jwt);
                setUser(response.data.user);
                // Store the JWT + user in the local storage
                localStorage.setItem("jwt", response.data.jwt);
                localStorage.setItem("user", JSON.stringify(response.data.user));
            })
            .catch(function (error) {
                console.log(error);
            });
    };


    /**
     * Validate form and retrieve calls the getJWT function
     */
    const onSubmit = () => {
        setError(null);
        if (!email || email === "") {
            setError({field: "email", message: "Please enter your email"});
        } else if (!password || password === "") {
            setError({field: "password", message: "Please enter your password"});
        } else {
            // Retrieve the JWT
            getJWT(email, password);

            console.log("User info properly entered");
        }
    }

    return (
        <div>
            <h1>Login</h1>
            <Input
                type={"text"}
                value={email}
                setValue={setEmail}
                label="yourmail@mail.com"
                error={error?.field === "email"}
            />
            <Input
                type={"password"}
                value={password}
                setValue={setPassword}
                label="*********"
                error={error?.field === "password"}
            />
            {error && <div>Error: {error.message}</div>}
            <StyledButtons onClick={onSubmit}>Submit</StyledButtons>
            <Link to="/create">Sign Up</Link>
        </div>
    );
}

export default Login;
