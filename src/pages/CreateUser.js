import React, {useContext, useState} from "react";
import Input from "../components/Input";
import {Link, useHistory} from "react-router-dom";
import UserContext from "../contexts/UserContext";
import axios from "axios";
import styled from "styled-components";

const CreateUser = () => {

    const history = useHistory();

    const {setJWT} = useContext(UserContext);

    const [username, setUsername] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [bio, setBio] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);

    const StyledButtons = styled.button`
        color: white;
        background-color: #3d005c;
        box-shadow: 0px 0px 0px transparent;
        border: 0px solid transparent;
        text-shadow: 0px 0px 0px transparent;
        border-radius:25px;
        padding : 6px;
    `;

    const createUser = (username, email, password, firstname, lastname, bio) => {
        axios
            .post("https://strapi-crea.5ika.org/auth/local/register", {
                username: username,
                email: email,
                provider: "string",
                password: password,
                resetPasswordToken: "string",
                confirmed: false,
                blocked: false,
                role: "string",
                firstname: firstname,
                lastname: lastname,
                bio: bio
            })
            .then(function (response) {
                // Set the JWT retrieved
                setJWT(response.data.jwt);
                // Store the JWT in the local storage
                localStorage.setItem("jwt", response.data.jwt);
            })
            .catch(function (error) {
                console.log(error);
            });
    };


    /**
     * Validate form and retrieve calls the createUser function
     */
    const onSubmit = () => {
        setError(null);
        if (!email || email === "") {
            setError({field: "email", message: "Please enter your email"});
        } else if (!password || password === "") {
            setError({field: "password", message: "Please enter your password"});
        } else if (!firstname || firstname === "") {
            setError({field: "firstname", message: "Please enter your firstname"});
        } else if (!lastname || lastname === "") {
            setError({field: "lastname", message: "Please enter your lastname"});
        } else if (!password || password === "") {
            setError({field: "username", message: "Please enter your username"});
        } else if (!bio || bio === "") {
            setError({field: "bio", message: "Please enter your bio"});
        } else {
            // Creates the user
            createUser(username, email, password, firstname, lastname, bio);
            console.log("User info properly entered");

            // auto-Redirect to root
            history.push("/");
        }
    }

    return (
        <div className="CreateUser">
            <h1>Enter your informations</h1>
            <Input
                type={"text"}
                value={email}
                setValue={setEmail}
                label="yourmail@mail.com"
                error={error?.field === "email"}
            />
            <Input
                type={"password"}
                value={password}
                setValue={setPassword}
                label="*********"
                error={error?.field === "password"}
            />
            <Input
                type={"text"}
                value={firstname}
                setValue={setFirstname}
                label="Firstname"
                error={error?.field === "firstname"}
            />
            <Input
                type={"text"}
                value={lastname}
                setValue={setLastname}
                label="lastname"
                error={error?.field === "lastname"}
            />
            <Input
                type={"text"}
                value={username}
                setValue={setUsername}
                label="username"
                error={error?.field === "username"}
            />
            <Input
                type={"text"}
                value={bio}
                setValue={setBio}
                label="bio"
                error={error?.field === "bio"}
            />
            {error && <div>Error: {error.message}</div>}
            <br/>
            <StyledButtons onClick={onSubmit}>Submit</StyledButtons>
            <Link to="/">Back to login page</Link>
        </div>
    );
}

export default CreateUser;
