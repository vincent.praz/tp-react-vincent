import React, {useContext, useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom";
import UserContext from "../contexts/UserContext";
import axios from "axios";
import Input from "../components/Input";
import Feed from "../components/Feed";
import styled from "styled-components";

const ProfileCard = styled.div`
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.8);
  transition: 0.3s;
  border-radius: 5px;
  width: 50%;
  padding:10px;
  margin-top: 15px;
  background-color: #3E403D;
`;


const ProfileContainer = styled.div`
    color:white;
    position: absolute;
    top:0;
    background-color: #484647;
    width:75%;
    padding-left:15px;
`;

const StyledButtons = styled.button`
    color: white;
    background-color: #3d005c;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;
    border-radius:25px;
    padding : 6px;
`;

const StyledButtonsDelete = styled.button`
    color: white;
    background-color: #8B0000;
    box-shadow: 0px 0px 0px transparent;
    border: 0px solid transparent;
    text-shadow: 0px 0px 0px transparent;
    border-radius:25px;
    padding : 6px;
`;

const Profile = () => {
    let history = useHistory();
    const {posts, setPosts, userView, setUserView, user, setUser, jwt, setJWT} = useContext(UserContext);
    const [firstname, setFirstname] = useState(user?.firstname);
    const [updated, setUpdated] = useState(false);
    const [lastname, setLastname] = useState(user?.lastname);
    const [bio, setBio] = useState(user?.bio);
    const [error, setError] = useState({});
    const [displayPost, setDisplayPost] = useState([]);


    // If the user profile is the profile of the connected user, you are allowed to edit it
    const edit = (userView.username === user.username);

    console.log(posts);

    /**
     * Filter the posts to get only the user's post
     * @param posts the full postlist
     */
    const filterPosts = (posts) => {
        const postsToDisplay = [];
        posts.map((post) => {
            if (post.user && post.user.username === userView.username) {
                postsToDisplay.push(post);
            }
        });
        setDisplayPost(postsToDisplay);
    }

    useEffect(() => {
        filterPosts(posts);
    }, []);


    const updateUser = () => {
        if (user) {
            axios({
                method: "put",
                url: `https://strapi-crea.5ika.org/users/${user.id}`,
                headers: {
                    Authorization: `Bearer ${jwt}`,
                },
                data: {
                    firstname,
                    lastname,
                    bio,
                },
            })
                .then((response) => {
                    console.log(response);
                    setUser(response.data);
                    localStorage.setItem("user", JSON.stringify(response.data));
                    setUpdated(true);
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            console.log("Error: User undefined");
        }
    };


    /**
     * Deletes the user
     */
    const deleteUser = () => {
        //API CALL TO DELETE USER
        if (user) {
            axios({
                method: "delete",
                url: `https://strapi-crea.5ika.org/users/${user.id}`,
                headers: {
                    Authorization: `Bearer ${jwt}`,
                },
            })
                .then((response) => {
                    console.log(response);
                    setUser(response.data);
                    //Loging out
                    setJWT(null);
                    setUser(null);
                    setPosts(null);
                    setUserView(null);

                    // Empty localstorage
                    localStorage.removeItem("jwt");
                    localStorage.removeItem("userView");
                    localStorage.removeItem("posts");
                    localStorage.removeItem("user");


                    //REDIRECT TO LOGIN PAGE
                    history.push("/");
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            console.log("Error: User undefined");
        }
    }

    const onSubmit = () => {
        setError(undefined);
        if (!firstname || firstname === "")
            setError({field: "firstname", message: "Merci d'entrer un firstname"});
        else if (!lastname || lastname === "")
            setError({field: "lastname", message: "Merci d'entrer un lastname"});
        else {
            updateUser();
            history.push("/");
        }
    };


    return (
        <ProfileContainer>

            <h1>Profile of {userView.username}</h1>
            <Link to="/">Home</Link><br/><br/>
            {edit ?
                <>
                    <ProfileCard>
                        {updated && <p>Profile successfully updated</p>}

                        <Input
                            value={firstname}
                            setValue={setFirstname}
                            label="Entrer your firstname"
                            type="text"
                            error={error?.field === "mail"}
                        />
                        <Input
                            value={lastname}
                            setValue={setLastname}
                            label="Entrer your lastname"
                            type="text"
                            error={error?.field === "mail"}
                        />
                        <Input
                            value={bio}
                            setValue={setBio}
                            label="Entrer your bio"
                            type="text"
                            error={error?.field === "mail"}
                        />
                        {error && <div>Error: {error.message}</div>}
                        <StyledButtons onClick={onSubmit}>Submit</StyledButtons>
                    </ProfileCard>
                    <br/>
                    <StyledButtonsDelete onClick={deleteUser}>DELETE MY ACCOUNT</StyledButtonsDelete>
                </> :
                <ProfileCard>
                    <p>Firstname : {userView.firstname}</p>
                    <p>Lastname : {userView.lastname}</p>
                    <p>Bio : {userView.bio}</p>
                </ProfileCard>
            }


            <h2>{userView.username}'s posts</h2>
            {displayPost.length !== 0 ?
                <>
                    {displayPost?.map((post, i) => (
                        <Feed key={i} post={post}/>
                    ))}
                </> : <p>{userView.username} didn't post anything yet !</p>
            }
        </ProfileContainer>
    );
};

export default Profile;
